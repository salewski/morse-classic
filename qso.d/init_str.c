char const *Transceiver[] = {
#include "rig.i"
    0};

char const *Antenna[] = {
#include "antenna.i"
    0};

char const *UpFeet[] = {
    "10",      "15",     "20",     "25",  "30", "35",  "40",     "45",
    "50",      "55",     "60",     "65",  "70", "75",  "80",     "85",
    "90",      "95",     "100",    "10",  "11", "12",  "13",     "14",
    "15",      "16",     "17",     "18",  "19", "20",  "10 1/2", "11 1/2",
    "25",      "29 1/2", "30",     "35",  "40", "42",  "45",     "45 1/2",
    "45.5",    "50",     "55",     "60",  "62", "65",  "70",     "75",
    "80",      "85",     "87 1/3", "90",  "95", "100", "103",    "137",
    "189 1/2", "210",    "246",    "250", 0};

char const *Weather1[] = {"sunny", "rain",   "freezing rain", "sleet",
                          "snow",  "cloudy", "partly cloudy", "partly sunny",
                          "clear", 0};

char const *Weather2[] = {
#include "weather.i"
    0};

char const *Power[] = {"2",   "5",   "10",  "20",  "25",  "40",
                       "50",  "80",  "100", "125", "140", "150",
                       "170", "200", "250", "270", "300", 0};

char const *Job[] = {
#include "jobs.i"
    0};

char const *Name[] = {
#include "names.i"
    0};

char const *CallSign[] = {
#include "callsign.i"
    0};

char const *License[] = {"Novice",    "Technician",
                         "Tech",      "Tech plus",
                         "Tech plus", "Tech plus",
                         "Tech plus", "Tech no code",
                         "General",   "Advanced",
                         "Extra",     "Extra",
                         "Extra",     0};

char const *City[] = {
#include "city.i"
    0};

char const *NewCity[] = {
#include "newcity.i"
    0};

char const *CityHeights[] = {
#include "cityh.i"
    0};

char const *New[] = {"New",
                     "Old",
                     "North",
                     "South",
                     "East",
                     "West",
                     "New",
                     "Old",
                     "North",
                     "Northeast",
                     "Northwest",
                     "South",
                     "Southwest",
                     "Southeast",
                     "East",
                     "West",
                     "Upper",
                     "Lower",
                     0};

char const *Heights[] = {
#include "heights.i"
    0};

char const *State[] = {"Alabama",
                       "Alaska",
                       "Arizona",
                       "Arkansas",
                       "California",
                       "Colorado",
                       "Connecticut",
                       "District of Columbia",
                       "Delaware",
                       "Florida",
                       "Georgia",
                       "Guam",
                       "Hawaii",
                       "Idaho",
                       "Illinois",
                       "Indiana",
                       "Iowa",
                       "Kansas",
                       "Kentucky",
                       "Louisiana",
                       "Maine",
                       "Maryland",
                       "Massachusetts",
                       "Michigan",
                       "Midway",
                       "Minnesota",
                       "Mississippi",
                       "Missouri",
                       "Montana",
                       "Nebraska",
                       "Nevada",
                       "New Hampshire",
                       "New Jersey",
                       "New Mexico",
                       "New York",
                       "North Carolina",
                       "North Dakota",
                       "Ohio",
                       "Oklahoma",
                       "Oregon",
                       "Pennsylvania",
                       "Puerto Rico",
                       "Rhode Island",
                       "Saipan",
                       "American Samoa",
                       "South Carolina",
                       "South Dakota",
                       "Tennessee",
                       "Texas",
                       "Utah",
                       "Vermont",
                       "Virginia",
                       "Virgin Islands",
                       "Wake Island",
                       "Washington",
                       "West Virginia",
                       "Wisconsin",
                       "Wyoming",
                       0};

char const *Frqmisc[] = {"QSN %d?", "QSU %d?", "QSW %d?", "QSN %d",
                         "QSU %d",  "QSW %d",  0};

char const *Callmisc[] = {"QRK %s",  "QRL %s",  "QRZ %s",  "QSP %s", "QRK %s?",
                          "QRL %s?", "QRZ %s?", "QSP %s?", 0};

char const *NumMisc[] = {"QRI %d",  "QRK %d",  "QRM %d",  "QRN %d",  "QRS %d",
                         "QRY %d",  "QSG %d",  "QTA %d",  "QTC %d",  "QRI %d?",
                         "QRK %d?", "QRM %d?", "QRN %d?", "QRS %d?", "QRY %d?",
                         "QSG %d?", "QTA %d?", "QTC %d?", 0};

char const *Miscellaneous[] = {
#include "misc.i"
    0};
char const *RST[] = {"555", "577", "578", "579", "588", "589",
                     "599", "478", "354", "248", "126", 0};
